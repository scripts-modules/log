
if [ -x /usr/bin/logger ]
then
    LOGGER=/usr/bin/logger
elif [ -x /bin/logger ]
then
    LOGGER=/bin/logger
else
    LOGGER=/usr/bin/echo
fi


log () {
   msg="${1}"
   user="${LOGNAME}"

   # TODO : faire un mode debug
   # printf -- ${msg}
   /usr/bin/timeout 2 $LOGGER -p local0.notice \
      -t "${PROG_NAME}" "${user}: ${msg} ($(dirname "${PROG_NAME}"))"
}

#
# Information
#
RESTORE='\e[0m'
RED='\e[31m'
GREEN='\e[38;5;64m'
YELLOW='\e[33m'
BLUE='\e[34m'


# implements: https://no-color.org/
NO_COLOR="${NO_COLOR:-}"
# Implements: https://bixense.com/clicolors/
CLICOLOR="${CLICOLOR:-1}"
CLICOLOR_FORCE="${CLICOLOR_FORCE:-0}"

if [ -n "${NO_COLOR}" ] \
	|| [ "${CLICOLOR}" == "0" ] && tty -s && [ "${CLICOLOR_FORCE}" == "0" ]
then
	COLOR_CHAPTER=""
	COLOR_TITLE1=""
	COLOR_ERROR=""
	COLOR_INFO=""
	COLOR_ACTION=""
	COLOR_WARNING=""
	COLOR_TODO=""
	RESTORE=""
else

    COLOR_CHAPTER='\e[33;48;5;52m['
    COLOR_TITLE1='\e[7;49;37m['
    COLOR_ERROR=${RED}
    COLOR_INFO=${BLUE}
    COLOR_ACTION=${GREEN}
    COLOR_WARNING=${RED}
    COLOR_TODO=${BLUE}

fi


chapter  () {
	printf -- "${COLOR_CHAPTER}% 100s\r[#] %s${RESTORE}\n" " " "${*}"
}

title1  () {
	printf -- "${COLOR_TITLE1}% 100s\r |  %s${RESTORE}\n" " " "${*}"
}

error () {
	printf -- "${COLOR_ERROR}[-] %s${RESTORE} \n" "${*}"
    log "ERROR: ${1}"
}

warning () {
	printf -- "${COLOR_WARNING}[!]${RESTORE} %s\n" "${*}"
}

action () {
	printf -- "${COLOR_ACTION}[+] %s${RESTORE}\n" "${*}"
}


info () {
	printf -- "${COLOR_INFO} |->${RESTORE} %s\n" "${*}"
}

todo () {
	printf -- "${COLOR_TODO}[ ] TODO: %s${RESTORE}\n" "${*}"
}


